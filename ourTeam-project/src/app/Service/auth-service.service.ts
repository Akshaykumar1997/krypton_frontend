import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})


export class AuthServiceService {
  showHide : boolean = true;
  constructor( private router : Router, private http : HttpClient ) { }

  setToken (token:string){
    localStorage.setItem('token',token);
  }

  getToken(){
    return localStorage.getItem('token');
  }

  isLoggedIn(){
    return this.getToken() !== null;
  }

  CustomerLoggedOut(){
    localStorage.removeItem('token');
    this.router.navigate(['customer-login']);
  }

  customerLogin({email,password}:any){

    if (email === "customer@gmail.com" && password === "Customer@123") {
      this.setToken('customerLoggedIn');
      console.log(email, password);
    }

    return throwError(() => new Error('Failed to login'));

  }


  


}
