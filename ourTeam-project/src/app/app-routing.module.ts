import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './landing-page/main/main.component';
import { CustomerLoginComponent } from './login-system/customer-login/customer-login/customer-login.component';
import { CustomerSignUpComponent } from './login-system/customer-login/customer-sign-up/customer-sign-up.component';
import { EmployeeLoginComponent } from './login-system/employee-login/employee-login/employee-login.component';
import { AuthGuard } from './guards/auth.guard';
import { CustomerModule } from './customer/customer.module';
import { CustomerRoutingModule } from './customer/customer-routing.module';

const routes: Routes = [
  {path:'home-page' , component:MainComponent},
  {path:'employee-login' , component:EmployeeLoginComponent},
  {path:'customer-login' , component:CustomerLoginComponent},
  {path:'customer-signup' , component:CustomerSignUpComponent},
  {path:'', redirectTo:'home-page',pathMatch:'full'},

  {path:'customer',
  canActivate:[AuthGuard],
  loadChildren:()=>import('./customer/customer.module').then((m)=>m.CustomerModule)},
  {path:'bank-employee',
  canActivate:[AuthGuard],
  loadChildren:()=>import('./employee/employee.module').then((m)=>m.EmployeeModule)},
  // {path:'**',component: MainComponent}  


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
