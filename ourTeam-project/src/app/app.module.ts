import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { HeaderComponent } from './landing-page/header/header.component';
import { MainComponent } from './landing-page/main/main.component';
import { FooterComponent } from './landing-page/footer/footer.component';

import { CustomerLoginComponent } from './login-system/customer-login/customer-login/customer-login.component';
import { CustomerSignUpComponent } from './login-system/customer-login/customer-sign-up/customer-sign-up.component';
import { CustomerForgetPasswordComponent } from './login-system/customer-login/customer-forget-password/customer-forget-password.component';
import { EmployeeLoginComponent } from './login-system/employee-login/employee-login/employee-login.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    HeaderComponent,
    MainComponent,
    FooterComponent,
    CustomerLoginComponent,
    CustomerSignUpComponent,
    CustomerForgetPasswordComponent,
    EmployeeLoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
