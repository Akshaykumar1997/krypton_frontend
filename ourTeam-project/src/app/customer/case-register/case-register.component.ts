import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';
import { Router } from '@angular/router';
import { Validators } from '@angular/forms';
import { amountDifference } from 'src/app/validators/amount.validator';
import { AuthServiceService } from 'src/app/Service/auth-service.service';
@Component({
  selector: 'app-case-register',
  templateUrl: './case-register.component.html',
  styleUrls: ['./case-register.component.css']
})
export class CaseRegisterComponent implements OnInit {

  caseData: any;
  caseDetails: any;

  constructor(private router: Router) { }

  ngOnInit() {

    //Reactive Form Controls
    this.caseDetails = new FormGroup(
      {
        cardHolderName: new FormControl('', [Validators.required, Validators.minLength(2), Validators.pattern('[a-zA-Z]*')]),
        cardNo: new FormControl('', [Validators.required, Validators.pattern('[0-9]*'), Validators.minLength(16)],),
        accountNo: new FormControl('', [Validators.required, Validators.pattern('[0-9]*'), Validators.minLength(9)]),
        typeOfAccount: new FormControl('null', Validators.required),
        merchantName: new FormControl('', [Validators.required, Validators.pattern('[a-zA-Z]*')]),
        transactionAmount: new FormControl('', [Validators.required, Validators.pattern('[0-9]*'), Validators.minLength(2)]),
        transactionDate: new FormControl('', [Validators.required]),
        transactionType: new FormControl('null', Validators.required),
        bankBal: new FormControl('', [Validators.required, Validators.pattern('[0-9]*'), Validators.minLength(2)]),
     

        // Questionaries
        card_cancelled: new FormControl('null', Validators.required),
        possession_when_fraudOccured: new FormControl('null', Validators.required),
        pin_shared: new FormControl('null', Validators.required),
        desc_fraudOccured: new FormControl('', [Validators.required, Validators.minLength(20)]),
        desc_transactionOccured: new FormControl('', [Validators.required, Validators.minLength(20)]),
        desc_additionalInfo: new FormControl('',),
      },
      {
        validators : amountDifference
      }
    );
  
  }

  onSubmit() {
    this.caseData = this.caseDetails.value;
    console.log(this.caseData);
    this.router.navigate(['/customer/home']);
    this.caseDetails.reset();
  
  }

  // calling controls OF the form for validations
  get err() { return this.caseDetails.controls };

}


