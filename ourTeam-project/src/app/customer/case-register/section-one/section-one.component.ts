import { Component, OnInit } from '@angular/core';
import { FormGroup, FormGroupDirective } from '@angular/forms';

@Component({
  selector: 'app-section-one',
  templateUrl: './section-one.component.html',
  styleUrls: ['../case-register.component.css']
})
export class SectionOneComponent implements OnInit {
sectionOneForm! : FormGroup ;
  constructor(private rootFormGroup : FormGroupDirective) { }

  ngOnInit(): void {
    this.sectionOneForm = this.rootFormGroup.control;

  }

  get err(){return this.sectionOneForm.controls}

}
