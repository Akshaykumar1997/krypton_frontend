import { Component, OnInit } from '@angular/core';
import { FormGroup, FormGroupDirective } from '@angular/forms';



@Component({
  selector: 'app-section-three',
  templateUrl: './section-three.component.html',
  styleUrls: ['../case-register.component.css']
})



export class SectionThreeComponent implements OnInit {

  getQuestionaries = [
    {
      "id": "1",
      "title": "Was the clients card cancelled?"

    },
    {
      "id": "2",
      "title": "Was your card in your possession when the transaction occured?"
    },
    {
      "id": "3",
      "title": "Was your pin ever written down or shared to someone else?"
    },
    {
      "id": "4",
      "title": "Was your card lost or stolen and where if know (how was card last or stolen and provide date and time and location)?"
    },
    {
      "id": "5",
      "title": "Where were you when the transaction occurred (ie: clientslocation)?"
    },
    {
      "id": "6",
      "title": "Please provide any additional information?"
    }
  ];

  sectionThreeForm!: FormGroup;

  constructor(private rootFormGroup: FormGroupDirective) { }

  ngOnInit(): void {
    this.sectionThreeForm = this.rootFormGroup.control;
  }

  get err() { return this.sectionThreeForm.controls };

}
