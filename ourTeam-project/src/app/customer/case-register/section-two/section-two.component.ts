import { Component, OnInit } from '@angular/core';
import { FormGroup, FormGroupDirective } from '@angular/forms';
import { amountDifference } from 'src/app/validators/amount.validator';

@Component({
  selector: 'app-section-two',
  templateUrl: './section-two.component.html',
  styleUrls: ['../case-register.component.css']
})
export class SectionTwoComponent implements OnInit {
  sectionTwoForm!: FormGroup
  constructor(private rootFormGroup: FormGroupDirective) { }

  maxDate: any;


  ngOnInit(): void {
    this.sectionTwoForm = this.rootFormGroup.control;

    this.futureDateDisable();

    

  }

  get err() { return this.sectionTwoForm.controls }

  futureDateDisable() {
    let date: any = new Date();
    let todayDate: any = date.getDate();
    let month: any = date.getMonth();
    let year: any = date.getFullYear();

    if (todayDate < 10) {
      todayDate = '0' + todayDate;
    }
    if (month < 10) {
      month = '0' + month;
    }

    this.maxDate = (year + '-' + month + '-' + todayDate);
  }
}
