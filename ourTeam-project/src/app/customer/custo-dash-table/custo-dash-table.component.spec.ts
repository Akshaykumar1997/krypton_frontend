import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustoDashTableComponent } from './custo-dash-table.component';

describe('CustoDashTableComponent', () => {
  let component: CustoDashTableComponent;
  let fixture: ComponentFixture<CustoDashTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustoDashTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustoDashTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
