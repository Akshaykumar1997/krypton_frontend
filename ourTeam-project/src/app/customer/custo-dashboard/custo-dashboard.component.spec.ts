import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustoDashboardComponent } from './custo-dashboard.component';

describe('CustoDashboardComponent', () => {
  let component: CustoDashboardComponent;
  let fixture: ComponentFixture<CustoDashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustoDashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustoDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
