import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustoDashboardComponent } from './custo-dashboard/custo-dashboard.component';
import { CaseRegisterComponent } from './case-register/case-register.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {path:'', component:CustoDashboardComponent,children:[
    {path:'home', component:HomeComponent},
    {path:'register-case', component:CaseRegisterComponent},
  ]},
  {}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRoutingModule { }
