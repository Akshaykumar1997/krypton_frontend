import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { CustomerRoutingModule } from './customer-routing.module';
import { CustoDashboardComponent } from './custo-dashboard/custo-dashboard.component';
import { CustoDashTableComponent } from './custo-dash-table/custo-dash-table.component';
import { CaseRegisterComponent } from './case-register/case-register.component';
import { HomeComponent } from './home/home.component';
import { SectionOneComponent } from './case-register/section-one/section-one.component';
import { SectionTwoComponent } from './case-register/section-two/section-two.component';
import { SectionThreeComponent } from './case-register/section-three/section-three.component';


@NgModule({
  declarations: [
    CustoDashboardComponent,
    CustoDashTableComponent,
    CaseRegisterComponent,
    HomeComponent,
    SectionOneComponent,
    SectionTwoComponent,
    SectionThreeComponent
  ],
  imports: [
    CommonModule,
    CustomerRoutingModule,
    ReactiveFormsModule,

    
  ]
})
export class CustomerModule { }
