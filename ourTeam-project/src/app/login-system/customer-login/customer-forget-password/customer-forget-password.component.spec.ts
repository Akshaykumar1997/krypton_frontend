import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerForgetPasswordComponent } from './customer-forget-password.component';

describe('CustomerForgetPasswordComponent', () => {
  let component: CustomerForgetPasswordComponent;
  let fixture: ComponentFixture<CustomerForgetPasswordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomerForgetPasswordComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerForgetPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
