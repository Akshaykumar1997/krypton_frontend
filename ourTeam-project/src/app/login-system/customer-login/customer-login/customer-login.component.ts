import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpStatusCode } from '@angular/common/http';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthServiceService } from 'src/app/Service/auth-service.service';


@Component({
  selector: 'app-customer-login',
  templateUrl: './customer-login.component.html',
  styleUrls: ['./customer-login.component.css']
})
export class CustomerLoginComponent implements OnInit {

  customerLoginForm = new FormGroup(
    {
      email: new FormControl(''),
      password: new FormControl(''),
    }
  );

  constructor( private customerAuth : AuthServiceService ,private router: Router,private http:HttpClient) { }

  ngOnInit(): void {
  }

  // onSubmit() {};

  postLogin() {

    if (this.customerLoginForm.valid) {
      // console.log(this.customerLoginForm.value);

      this.customerAuth.customerLogin(this.customerLoginForm.value);
      this.router.navigate(['/customer/home']);
      // this.customerAuth.customerLogin(this.customerLoginForm.value).subscribe({
      //   next:()=>{this.router.navigate(['/customer/home']);
      //   console.log("logedin");
      // },
      //   error:(err:Error)=>{
      //     console.log(err);
      //   }
      // })
    }

    // this.router.navigate(['/customer/home']);
  }


}