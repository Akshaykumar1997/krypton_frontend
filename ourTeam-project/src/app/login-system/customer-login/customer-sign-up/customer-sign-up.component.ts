import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { matchPassword } from 'src/app/validators/password.validator';

// import { customValidator } from './customValidator';

@Component({
  selector: 'app-customer-sign-up',
  templateUrl: './customer-sign-up.component.html',
  styleUrls: ['./customer-sign-up.component.css']
})
export class CustomerSignUpComponent implements OnInit {

  signUpForm!: FormGroup;

  postUserData : any ;

  constructor(public formBuilder: FormBuilder) {

    this.signUpForm = this.formBuilder.group({
      firstName: new FormControl('', [Validators.required, Validators.minLength(2), Validators.pattern('[a-zA-Z]*')]),
      lastName: new FormControl('', [Validators.required, Validators.minLength(2), Validators.pattern('[a-zA-Z]*')]),
      email: new FormControl('', [Validators.required, Validators.email]),
      phoneNumber: new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern('[0-9]*')]),
      gender: new FormControl('', Validators.required),
      panCard: new FormControl('', [Validators.required, Validators.maxLength(10), Validators.pattern('[a-zA-Z0-9]*')]),
      address: new FormControl('', [Validators.required, Validators.minLength(25)]),
      password: new FormControl('', [Validators.required, Validators.pattern('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$')]),
      confirmPassword: new FormControl('', [Validators.required])

    },
      {
        validators: matchPassword
      }
    )

  }

  ngOnInit() {  }

  get signUpError (){return this.signUpForm.controls};







  onSubmit() { 

    this.postUserData = this.signUpForm.value;

    console.log(this.postUserData);
   }

}
