import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";




export const amountDifference: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    let bankBalPriorFraud = control.get('bankBal');
    let bankBalAfterFraud = control.get('transactionAmount');

    if ( bankBalPriorFraud && bankBalAfterFraud && +(bankBalAfterFraud?.value) > +(bankBalPriorFraud?.value)) {
        return {
            amountDifferenceError: true
        }
    }

    return null;
}

