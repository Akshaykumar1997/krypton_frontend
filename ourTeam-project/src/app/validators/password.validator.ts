import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";




export const matchPassword: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    let Password = control.get('password');
    let ConfirmPassword = control.get('confirmPassword');

    if (Password && ConfirmPassword && Password?.value !== ConfirmPassword?.value) {
        return {
            passwordmatcherror: true
        }
    }
    return null;
}